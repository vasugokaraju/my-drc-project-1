/**
 * Created by vasu on 10/4/17.
 */

console.log('**** Dharma Robotics Class ***')
console.log('\nRobot launch sequence is commenced.\n')

// Load the http module to create an http server.
//var http = require('http');
var globals = require('./common/globals');
var GPIO = require('pigpio').Gpio;

var odo;
var keyboard;
//var webServer;

var sensorData;
//var dataRepo;
var jobManager;


try {
    globals.leftPWM = new GPIO(globals.defaultHBridgeSettings.PWM1Pin, {mode: GPIO.OUTPUT})
    globals.rightPWM = new GPIO(globals.defaultHBridgeSettings.PWM2Pin, {mode: GPIO.OUTPUT})

    // Load the modules only if system is not occupied.
    odo = require('./common/odometer');                 // Load Odometer module to measure distance travelled.
    sensorData = require('./automatic/sensorData');     // Load SensorData module to handle incoming data from phone
//    dataRepo = require('./automatic/dataRepo');     //
    jobManager = require('./automatic/jobManager');     // Loads job management system and runs the vehicle as per data provided in google spreadsheet.

//    webServer = require('./manual/webServer');     // Load SensorData module to handle incoming data from phone
    keyboard = require('./manual/keyboard');            // Load Keyboard module to handle keyboard navigation
}
catch (e) {
    globals.clearConsole();
    console.log('launch exception', e);

    // https://ourcodeworld.com/articles/read/298/how-to-show-colorful-messages-in-the-console-in-node-js
    console.log('\x1b[31m%s\x1b[0m', '\n\n*** ROBOT IS ALREADY IN USE. TERMINATE THE RUNNING SESSION AND TRY AGAIN ***.\n\n');
    return;
}


//// Configure our HTTP server to respond with Hello World to all requests.
//var server = http.createServer(function (request, response) {
//    response.writeHead(200, {"Content-Type": "text/plain"});
//    response.end("Hello World\n");
//
//});
//
//// Listen on port 8000, IP defaults to 127.0.0.1
//server.listen(8000);
//
//// Put a friendly message on the terminal
//console.log("Server running at http://127.0.0.1:8000/");

console.log('\n\n*****************************************************');
console.log('* Press Ctrl + C to exit and return to command line *');
console.log('*****************************************************\n\n');

console.log('Robot IP Addresses:')
globals.ipAddresses = globals.getIPAddresses();
globals.ipAddresses.forEach( function(item, indx) {
    console.log(item.interfaceName, '>> ', item.ip)
});
console.log('\n');

console.log('Robot Dashboard URL: %s\n', globals.dashboardURL)

// This call prints available IP addresses
// Move this to right place
globals.getIPAddresses();