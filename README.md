# README #

This is a simple project that controls robot through arrowkeys on keyboard.  

# PREREQUISITES #
You should have Raspberry Pi with Node.js installed.  Use the preinstalled operating system image from the class.
Make sure HBridge is in place and wiring is completed.

# INSTALLATION #
Clone this project into a new folder of your choice.
Run the command "node index.js"
Now press arrow keys to run the four wheel robot.