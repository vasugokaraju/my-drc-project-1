/**
 * Created by vasu on 10/25/17.
 */

var globals = require('../common/globals');
//https://www.twilio.com/blog/2017/03/google-spreadsheets-and-javascriptnode-js.html
var GoogleSpreadsheet = require('google-spreadsheet');
var moment = require('moment-timezone');

// spreadsheet key is the long id in the sheets URL
// Make sure you shared this file with big long strange email that you find in client_email in credentials file.
var doc = new GoogleSpreadsheet('1zfzZKe825KmKc5NeCPoZ23rpPI3EZ_ORlvHAH6ShCS4');
var creds = require('./google-api-creds-c3bb83810702.json');

// Periodically upload data to cloud
var dataUploadTimer;

module.exports = {
//    initialized:false,

    initialize: function(cb){

//        console.log('Google Spreadsheet URL:', "https://docs.google.com/spreadsheets/d/1zfzZKe825KmKc5NeCPoZ23rpPI3EZ_ORlvHAH6ShCS4")

        doc.useServiceAccountAuth(creds, function(err, resp){

            if(err){
                console.log('Error while authenticating to Google Spreadsheets');
                console.log(err);
            }

            doc.getInfo(function(err, info) {

                if(err){
                    console.log('Error while retrieving Google Spreadsheet');
                    console.log(err);
                }

                globals.sheets = info.worksheets;
                globals.dashboardSheet = globals.sheets[globals.dashboardSheetIndex];

//                module.exports.initialized=true;

                cb(err, info);
            });

        });
    },

    // Returns the names of the routes (sheets)
    getRoutesList: function(){
        var routesInfo = [];
        globals.sheets.forEach(function(item, indx, list){
            routesInfo.push({index:indx, name:item.title});
        })

        // Remove the first sheet as it is dashboard.
        routesInfo.shift();

        return routesInfo;
    },

    getUserSelectedRouteMetaData: function(cb){
        module.exports.getCells(0,3,4, function(err, cells){

            if(!err) {
//                cells.forEach(function(cell, indx){
//                    console.log('Cell Index: %s, Row: %s, Col: %s, Value: %s',indx, cell.row, cell.col, cell.value);
//                })

                globals.userSelectedRouteMetaData = cells;
                globals.userSelectedJobName = cells[2].value;
                globals.userSelectedJobStatus = cells[5].value;
                globals.jobStatusCell = globals.userSelectedRouteMetaData[5];


                // Need spreadsheet index to retrieve data.  Get the index based on the sheet name.
                var userSelectedRoute = globals.routesList.find( function(item){
                    return item.name == globals.userSelectedJobName
                })
                globals.userSelectedRouteSheetIndex = userSelectedRoute.index;

                cb(null, globals.userSelectedRouteMetaData);
            }
            else{
                console.log('Error in retrieving user selected route info.');
                cb(err,null)
            }

        });
    },

    // Returns full data set of the route in JSON format
    getRouteData: function(sheetIndex, rowsOffset, rowsLimit, cb){
        var sheet = globals.sheets[sheetIndex];
        var data = [];
        var routeInfo = {index:sheetIndex, title: sheet.title}

        sheet.getRows({offset:rowsOffset, limit:rowsLimit}, function(err, rows){

            rows.forEach(function(item,indx){
                data.push({segment:item.segment, angle:item.angle, distance:item.distance, speed:item.speed, comments:item.comments });
            })

            routeInfo.data = data;
            cb(err, routeInfo);
        })
    },

    // Retrieve given range of cells
    getCells: function(sheetIndex, minRow, maxRow, cb){
        var cellsConfig = {
            'min-row': minRow,
            'max-row': maxRow,
            'return-empty': false
        }

        globals.sheets[sheetIndex].getCells(cellsConfig, function(err, cells){
            if(err){
                cb(err,null)
            }
            else{
                cb(null, cells)
            }

        })

    },

    // Periodically update dashboard.
    updateDashboard: function () {
        var updateFlag=true;
        
        dataUploadTimer = setInterval(function () {

            // If wheels are not moving, no need to update dashboard
//            if((globals.currentSpeedDirection.PWM1Val == undefined || globals.currentSpeedDirection.PWM1Val==0) &&
//                globals.currentSpeedDirection.PWM2Val == undefined || globals.currentSpeedDirection.PWM2Val==0 && !updateFlag) {
//                return;
//            }

            var dt = new Date();
            var m = moment.tz(new Date(), 'America/Chicago');   // convert using the TZDB identifier for US Central time

            // There MUST be some value in the spreadsheet cells otherwise cell count will not match as empty cells are not counted.

            // Get individual cells as objects to update
            var routeNameCell = globals.dashboardCells[2];
            var compassAngleCell = globals.dashboardCells[5];
            var desiredAngleCell = globals.dashboardCells[8];
            var currentDirectionCell = globals.dashboardCells[11];

            var segDistanceCell = globals.dashboardCells[14];
            var totalDistanceCell = globals.dashboardCells[17];
            var leftSpeedCell = globals.dashboardCells[20];
            var rightSpeedCell = globals.dashboardCells[23];
            var ipAddressCell = globals.dashboardCells[26];
            var lastModifiedTimeCell = globals.dashboardCells[29];
            var commentsCell = globals.dashboardCells[32];

            // Update individual cell objects to further update the google spreadsheet
            routeNameCell.value = globals.sheets[globals.userSelectedRouteSheetIndex].title;
            compassAngleCell.numericValue = globals.compassAngle;
            desiredAngleCell.numericValue = globals.desiredAngle;
            currentDirectionCell.value = (globals.currentDirection.toUpperCase() || 'STOP');
            segDistanceCell.numericValue = Math.round(globals.segmentDistance * 100) / 100;
            totalDistanceCell.numericValue = globals.totalDistance;
            leftSpeedCell.numericValue = (globals.currentSpeedDirection.PWM1Val||0);    // Left wheels
            rightSpeedCell.numericValue =  (globals.currentSpeedDirection.PWM2Val||0);  // Right wheels
            ipAddressCell.value = globals.ipAddresses[0].ip;
            lastModifiedTimeCell.value = "'" + m.format("MMM/DD/YY HH:mm:ss");   //  m.format("dddd MMM DD YYYY HH:mm:ss")
            commentsCell.value = globals.comments;

            globals.dashboardSheet.bulkUpdateCells(globals.dashboardCells, function () {
                //console.log('Dashboard is updated.');
                // If wheels are not moving, set the flag to false to prevent dashboard update from NEXT TIME onwards
                updateFlag = (leftSpeedCell.numericValue==0)?false:true;
            });

        }, globals.dashboardUpdateFrequency);

    },

    updateJobStatus: function(status){
        globals.userSelectedJobStatus = status;
        globals.jobStatusCell.value = status;

        globals.dashboardSheet.bulkUpdateCells(globals.userSelectedRouteMetaData, function (err, data) {
            console.log('Job status is updated.');
        });

    },

    // Stop updating dashboard
    stopDashboardUpdate: function () {
        clearInterval(dataUploadTimer);
    }

}