/**
 * Created by vasu on 10/27/17.
 */

var globals = require('../common/globals');
var navigation = require('../common/navigation');
var google = require('./google');


google.initialize(function (err, info) {
//    console.log('Document Title: ', info.title);

//    // Get the record of user selected route.
//    var userSelectedRoute = globals.routesList.find( function(item){
//        return item.name == globals.userSelectedJobName
//    })
//
//    if(userSelectedRoute){
//        google.updateDashboard();   // Start updating google dashboard periodically.
//
//    }
//    else{
//        console.log('Failed to find the user selected route from the list.', globals.userSelectedJobName, globals.routesList)
//    }


// Global interval function to manage route execution
    setInterval(function(){

        // If User selected route info is available and if the user sets the job status as 'Run', execute the job
        if(globals.userSelectedRouteMetaData && globals.userSelectedJobStatus == globals.JobStatus.Run){

            module.exports.getSelectedRouteInfo(userSelectedRoute.index, function(err, data){

                if(globals.userSelectedJobStatus == globals.JobStatus.Run){
                    globals.jobStatusCell.value = globals.JobStatus.Running;

                    // Start the job if selected by the user in spreadsheet
                    navigation.runJob(function(err,data){
                        globals.comments = 'JOB COMPLETED!';
                        console.log('globals.comments', globals.comments);
                    });
                }



            });


        }


    }, 2000)


});

module.exports = {
    // Gets a route data
    getSelectedRouteInfo: function (routeIndex, cb) {
        // Second sheet, offset 1, row limit 1000
        google.getRouteData(routeIndex, 1, 1000, function (err, data) {
            globals.selectedRoute = data;

            if(err){
                cb(err,null)
            }
            else{
                cb(null,data)
            }

            console.log('Selected Route:', globals.selectedRoute.title);
        });
    }

}


console.log('DataRepository module is loaded.');