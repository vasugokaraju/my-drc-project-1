/**
 * Created by vasu on 10/27/17.
 */

var globals = require('../common/globals');


// Start the server that listens for sensor data from phone through UDP protocol.
// ============================== UDP ==================================
const dgram = require('dgram');
const server = dgram.createSocket('udp4');

// Android sensor numbers
// 1=GPS, 3=Accelerometer, 4=Gyro, 5=Mag, 81=Orientation

server.on('message', function (msg, rinfo){

    globals.parseHyperIMUData(msg.toString(), function(data){
        globals.compassAngle = data.heading;
//        console.log('HyperIMU', data);
    });

});

server.on('listening', function() {
    var address = server.address();
    console.log('UDP server is listening at  %s:%s', address.address, address.port);
});


server.on('error', function(err) {
    console.log('UDP server error:', err.stack);
    server.close();
});

server.bind(globals.UDPPort);

// =====================================================================


console.log('SensorData module is loaded.');
