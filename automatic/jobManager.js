/**
 * Created by vasu on 7/18/18.
 */
var async = require('async');
var globals = require('../common/globals');
var navigation = require('../common/navigation');
var google = require('./google');

async.series({
        initializeGoogleDocs: function (cb) {
            google.initialize(function (err, info) {
                console.log('Google initialization is completed.')
                cb(err, info)
            });

        },
        retrieveRoutesList: function (cb) {
            // Get the list of routes (sheets)
            globals.routesList = google.getRoutesList();
            console.log('Routes list is retrieved.')
            cb(null, globals.routesList);
        },
        retrieveUserSelectedRouteInfo: function (cb) {
            // Get the user selected route
            google.getUserSelectedRouteMetaData(function (err, data) {
//                console.log('Retrieved user selected route meta data.')
                cb(err, data)
            });

        },
        retrieveDashboardCells: function (cb) {

            // Get Dashboard cells to update vehicle status.
            google.getCells(globals.dashboardSheetIndex, globals.dashboardRowOffset, globals.dashboardMaxRow, function (err, cells) {

                if (!err) globals.dashboardCells = cells;
                else {
                    console.log('Error in retrieving Dashboard data');
                }

//                globals.dashboardCells.forEach(function(cell, indx){
//                    console.log('Cell Index: %s, Row: %s, Col: %s, Value: %s',indx, cell.row, cell.col, cell.value);
//                })

                if (globals.userSelectedRouteSheetIndex > 0) {
                    console.log('Dashboard update process is started.')
                    google.updateDashboard();   // Start updating google dashboard periodically.

                }
                else {
                    console.log('Failed to find the user selected route from the list.', globals.userSelectedJobName, globals.routesList)
                }

                console.log('Retrieved dashboard data.')
                cb(err, cells);
            });
        }

    },
    function (err, results) {
        var gatekeeper = true;

        console.log('Checking for jobs to run.')

        // Start interval for route management
        setInterval(function () {

            // If User selected route info is available and if the user sets the job status as 'Run', execute the job
            if (globals.userSelectedJobStatus == globals.JobStatus.Run && gatekeeper) {
                gatekeeper = false;

                async.series({
                        retrieveRouteData: function (cb) {

                            // nth sheet, offset 1, row limit 1000
                            google.getRouteData(globals.userSelectedRouteSheetIndex, 1, 1000, function (err, data) {
                                globals.selectedRoute = data;

                                if (err) {
                                    console.log('Error in retrieving route data.', err)
                                }
                                else {
                                    cb(err, data)
                                }

                            });

                        }
                    },
                    function (err, data) {

                        google.updateJobStatus(globals.JobStatus.Running);

                        // Start the job if selected by the user in spreadsheet
                        navigation.runJob(function (err, data) {

                            google.updateJobStatus(globals.JobStatus.Completed);

                            globals.comments = 'JOB COMPLETED!';
                            console.log('globals.comments', globals.comments);
                            gatekeeper = true;
                        });

                    })
            }
            else{

                if(!globals.isVehicleMoving()){

                    // Get the user selected route
                    google.getUserSelectedRouteMetaData(function (err, data) {
//                        console.log('Retrieved user selected route meta data.!!')
                        //cb(err, data)
                    });

                }
            }

        }, 2000)

        console.log('Job Manager is ready.')
    });


console.log('JobManager module is loaded.');