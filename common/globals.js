/**
 * Created by vasu on 10/19/17.
 */

// Holds global variables
const wheelDiameter = 9;    // inches
const totalInterrupts = 40; // Interrupts on the wheel
const timeDiffThreshold = 200;              // Milliseconds. This value is used to determine whether the vehicle is moving.


module.exports = {
    distancePerInterrupt : (wheelDiameter / totalInterrupts), // Distance travelled per interrupt is 0.225 (7/32) inches.
    totalDistance:0,                    // Inches
    segmentDistance:0,                  // Inches

    minSpeed:90,                        // PWM range is between 0 to 255 but to make the motors run PWM should se set to 50 - 70 depends on voltage
    maxSpeed:255,                       // PWM max range
    currentSpeed:70,                    // Minimum speed (PWM) to make it work.
    currentSpeedDirection:{},           // Current HBridge
    currentDirection:'STOP',                // Holds string value that indicates the direction of the vehicle

    leftPWM:{},                         // Controls the speed of left wheels
    rightPWM:{},                        // Controls the speed of right wheels

    lastMotionTimestamp:new Date(),     // Latest vehicle movement timestamp.

    // Pin numbers represent GPIO pins
    defaultHBridgeSettings : {
                                IN1Pin:5,   // Pin index 29
                                IN2Pin:6,   // Pin index 31
                                IN3Pin:13,   // Pin index 33
                                IN4Pin:19,   // Pin index 35
                                PWM1Pin:23,     // GPIO 23 Right motors.  Pin index 16
                                PWM2Pin:24,     // GPIO 24 Left motors.   Pin index 18
                                IN1State:1,
                                IN2State:0,
                                IN3State:1,
                                IN4State:0,
                                PWM1Val:0,
                                PWM2Val:0
    },


    directionAlias : {
                        j:'left',
                        l:'right',
                        i:'up',
                        k:'down',
                        q:'stop',
                        escape:'stop',
                        J: 'wide_left',
                        L: 'wide_right'
    },

    JobStatus: {
        Run:'Run',
        Running: 'Running',
        Paused: 'Pause',
        Stop: 'Stop',
        Completed: 'Completed'
    },

    ipAddresses: [],                        // Holds a list of available IP addresses
    UDPPort: 2055,
    compassAngle:0,                         // Current heading angle
    desiredAngle:0,                         // desired angle

    webServerHost: '0.0.0.0',               // Webserver host ip. 0.0.0.0 accepts calls directed to any ip associated with device
    webServerPort: 3000,                    //

    routesList: [],                         // Holds a list of JSON objects with spreadsheet index and sheet name that represents the route.
    userSelectedJobName:'',
    userSelectedJobStatus:'',
    userSelectedRouteSheetIndex:-1,
    userSelectedRouteMetaData: {name:'', jobStatus:''},              // User selected route name and job status. Need this to find the index of the sheet for route data.
    selectedRoute:{},                        // Holds current route info
    jobStatusCell:'',                       // Reference to spreadsheet cell. Need it to update the job status to dashboard.


    sheets:[],                              // google spreadsheet sheets
    dashboardSheet:{},                      // Dashboard sheet to update various parameters
    dashboardCells:[],

    dashboardUpdateFrequency:1000,           // milliseconds
    dashboardRowOffset:9,
    dashboardMaxRow:24,
    dashboardSheetIndex:0,
    dashboardURL: 'https://goo.gl/4AomtS',

    jobInProgress:false,                    // Indicates whether any jobs are in progress

    comments:'Running comments about vehicle status.',                            // Running comments to show on dashboard.

    clearConsole: function(){
        process.stdout.write('\033c');  // Clears the console;
    },

    parseHyperIMUData : function(raw, cb){
        var data = raw.toString().replace('#','').split(',');

        // 'heading' should be calculated using all the x,y,z values for better accuracy.
        var sensorData = {timestamp: data[0], heading: parseFloat(data[1]).toFixed(0), lat:data[4], lon:data[5], alt:data[6] }

        module.exports.compassAngle = sensorData.heading;


        cb(sensorData);
    },

    getIPAddresses:function(){
        
        var os = require('os');
        var ifaces = os.networkInterfaces();
        var ipAddresses = [];

        Object.keys(ifaces).forEach(function (ifname) {
            var alias = 0;

            ifaces[ifname].forEach(function (iface) {
                if ('IPv4' !== iface.family || iface.internal !== false) {
                    // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                    return;
                }

                if (alias >= 1) {
                    // this single interface has multiple ipv4 addresses
//                    console.log(ifname + ':' + alias, iface.address);
                    ipAddresses.push({interfaceName:(ifname + ':' + alias), ip:iface.address});
                } else {
                    // this interface has only one ipv4 adress
//                    console.log(ifname, iface.address);
                    ipAddresses.push({interfaceName:ifname, ip:iface.address});
                }
                ++alias;
            });
        });

        return ipAddresses;
    },

    isVehicleMoving:function(){
        return !( (new Date().getTime() - module.exports.lastMotionTimestamp.getTime())  > timeDiffThreshold); // If time difference is greater than timeDiffThreshold milliseconds, consider it as vehicle not moving.
    }



}

//console.log('distancePerInterrupt', module.exports.distancePerInterrupt)
console.log('Globals module is loaded.');
