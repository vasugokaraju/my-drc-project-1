/**
 * Created by vasu on 10/18/17.
 */

var wpi = require('wiring-pi');
var globals = require('./globals');
//var nav = require('./navigation');

var speedSensorSettings = {
    pinA:17,             // GPIO 17 (pin index 11)
    pinB:19             // GPIO 19 (pin index 13)
}

wpi.setup('gpio');
wpi.pinMode(speedSensorSettings.pinA, wpi.INPUT);
wpi.pinMode(speedSensorSettings.pinB, wpi.INPUT);

wpi.pullUpDnControl(speedSensorSettings.pinA, wpi.PUD_UP);
wpi.pullUpDnControl(speedSensorSettings.pinB, wpi.PUD_UP);

try {
    wpi.wiringPiISR(speedSensorSettings.pinA, wpi.INT_EDGE_RISING, updateSpeedCounter);
//    wpi.wiringPiISR(speedSensorSettings.pinB, wpi.INT_EDGE_RISING, updateSpeedCounter);
}
catch (e) {
    console.log('Failed to run wiringPiISR', e.message)
}

module.exports = {
    resetTotalDistance: function(){
        globals.totalDistance =0;
    },

    resetSegmentDistance: function(){
        globals.segmentDistance=0;
    }
}


function updateSpeedCounter(){

//    var pinAReading = wpi.digitalRead(speedSensorSettings.pinA);   // Left

//    if(pinAReading == wpi.LOW){
//        interruptCounter++;
//    }
//    else{
//        interruptCounter--;
//    }

    globals.lastMotionTimestamp = new Date();

    globals.totalDistance += globals.distancePerInterrupt;
    globals.segmentDistance += globals.distancePerInterrupt;
//    globals.comments = 'Total Distance:' + globals.totalDistance.toFixed(1) + ' Heading:' + globals.compassAngle;
    //console.log(globals.comments)
//    if(globals.totalDistance >= 9) nav.drive('q');

}

console.log('Odometer module is loaded.');