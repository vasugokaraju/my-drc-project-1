/**
 * Created by vasu on 10/4/17.
 */

var _ = require('underscore');
var wpi = require('wiring-pi');
var GPIO = require('pigpio').Gpio;
var globals = require('./globals');

// Set pin mode as OUT and specify GPIO pin number instead of physical pin number
// The only function of this module is to turn some of the normal pins as PWM pins. It does not have the capability to set ON/OFF to pins.
// Only eligible GPIO pins for PWM are: 4,17,18,21,22,23,24,25
// https://github.com/hybridgroup/pi-blaster
//var piblaster = require('pi-blaster.js');   // Not working for pi 3 B.

var segIndex=0;
var tripCallback;
var currentSpeed;
var rotationSpeed = 170; // 0.25
var motorSpeedReduction = 20;     // % of speed to be reduced to bring the vehicle back to its desired angle
var int180 = 180;
var newAngleMissCount=0;                // Count the misses as the vehicle rotate to reach desired angle.
var hBridgeID;
const PWMMax = 250;


// ============================= SET PIN MODES =============================
wpi.setup('gpio');
wpi.pinMode(globals.defaultHBridgeSettings.IN1Pin, wpi.OUTPUT);       // Pin Mode must be OUTPUT
wpi.pinMode(globals.defaultHBridgeSettings.IN2Pin, wpi.OUTPUT);       // Pin Mode must be OUTPUT
wpi.pinMode(globals.defaultHBridgeSettings.IN3Pin, wpi.OUTPUT);       // Pin Mode must be OUTPUT
wpi.pinMode(globals.defaultHBridgeSettings.IN4Pin, wpi.OUTPUT);       // Pin Mode must be OUTPUT

try {
    globals.leftPWM = new GPIO(globals.defaultHBridgeSettings.PWM1Pin, {mode: GPIO.OUTPUT})
    globals.rightPWM = new GPIO(globals.defaultHBridgeSettings.PWM2Pin, {mode: GPIO.OUTPUT})
}
catch (e) {
    // https://ourcodeworld.com/articles/read/298/how-to-show-colorful-messages-in-the-console-in-node-js
    console.log('\x1b[31m%s\x1b[0m', '*** ROBOT IS ALREADY IN USE. TERMINATE OTHER SESSION AND TRY AGAIN ***.');
    return;
}

globals.leftPWM.pwmWrite(0);    // Set the default, otherwise it is giving error when trying to execute globals.leftPWM.getPwmDutyCycle()
globals.rightPWM.pwmWrite(0);

// ============================================================

module.exports = {

    drive: function(direction){

        direction = ( globals.directionAlias.hasOwnProperty(direction) )? globals.directionAlias[direction]:direction;
        globals.currentSpeed = (direction == 'stop')? 0 : Math.max( globals.minSpeed, globals.currentSpeed);    // Make sure there is enough speed (PWM pulse) otherwise the wheels do not rotate.

        if(direction == globals.currentDirection) return;

        globals.currentSpeedDirection = setSpeedDirection(direction, globals.currentSpeed);       // Set new hbridge settings
        driveVehicle_manual(globals.currentSpeedDirection);                                      // Apply hbridge settings

        // 12v is too high. Stick to 9v.

//        // When vehicle is given 12v and wheels are running at full speed, if the direction is changed to opposite, it is killing the battery.
//        // Delay safety when changed the wheel direction. Useful on high speeds but not sure whether it really helps.
//        // Find a way to solve this in circuit.
//        setTimeout(function(){
//            globals.currentSpeedDirection = setSpeedDirection(globals.currentSpeed, direction);       // Set new hbridge settings
//            driveVehicle_manual(globals.currentSpeedDirection);                                      // Apply hbridge settings
//        }, 500);

    },

    // Control speed
    speed: function (speedVal){

        var newSpeed = (globals.currentSpeed + speedVal);

        // Change speed value as long as it stays between 0 and 1.
        if(newSpeed >= 0 && newSpeed <= globals.maxSpeed){
            globals.currentSpeed = newSpeed;
        }
        else{
            return;
        }

//        globals.currentSpeed = round(globals.currentSpeed,1);     // Make sure there is only one decimal place

        // I think it is safe to stop the motors first and then change their direction.
        // Motors are making funny noise when its direction is changed at high speed.
//        globals.currentSpeedDirection = setSpeedDirection(0, globals.currentDirection);             // Set new hbridge settings
//        driveVehicle_manual(globals.currentSpeedDirection);                                      // Apply hbridge settings

        globals.currentSpeedDirection = setSpeedDirection(globals.currentDirection, globals.currentSpeed); // Set new hbridge settings
        driveVehicle_manual(globals.currentSpeedDirection);                                      // Apply hbridge settings

//        console.log('Left Wheel Speed:', globals.leftPWM.getPwmDutyCycle())
//        console.log('Right Wheel Speed:', globals.rightPWM.getPwmDutyCycle())
    },

    // Sets different speeds to left/right wheels to make slight navigational adjustments as the vehicle travels to reach desired angles smoothly.
    updateSpeeds: function(leftSpeed, rightSpeed){

    },


    // Run a job, if status is 'Run'
    runJob: function(cb){
        tripCallback = cb;
        segIndex = 0;

        // Check if the vehicle is already running
        if(!globals.jobInProgress){

            // If data is not available to run a job
            if(!globals.selectedRoute.data){
                console.log('No data is available to run the job. Will make an attempt in few seconds.')
            }
            else{
                process.nextTick(runSegment);
            }
        }
        else{
            console.log( '"' + globals.userSelectedJobName + '" job is in progress. Try again later.')
        }

    }
};

// Makes the vehicle travel a given segment of the route
function runSegment(){
    // If last segment is travelled, callback the main function and exit
    if(segIndex == globals.selectedRoute.data.length ){
        console.log('All segments are done.');

        // Stop the vehicle
        var stopSettings = setSpeedDirection('stop', 0);
//        piblaster.setPwm(defaultHBridgeSettings.PWM1Pin, 0);									// Specify GPIO pin number (not the physical pin number);
//        piblaster.setPwm(defaultHBridgeSettings.PWM2Pin, 0);									// Specify GPIO pin number (not the physical pin number);
        globals.leftPWM.pwmWrite(0);
        globals.rightPWM.pwmWrite(0);

        tripCallback(); // Final callback
        return;
    }

    console.log('**************Running segment ', (segIndex+1), 'of', globals.selectedRoute.data.length, '******************')

    var seg = globals.selectedRoute.data[segIndex];
    
    globals.desiredAngle = seg.angle;

    // If this is the first segment, use 360 as current angle, if not use previous segments degrees as current angle
    var degDir;


    if(segIndex==0){
        // If this is the first segment, use compass angle as current angle so that the vehicle can be turned to the angle as specified in first segment.
        // This helps to automatically turn the vehicle to right angle at the beginning of the trip.
        degDir = getNewAngleDistanceAndDirection(globals.compassAngle, seg.angle)
    }
    else{
        // If it is other than first segment, use previous segment's angle as current angle
        degDir = getNewAngleDistanceAndDirection(globals.selectedRoute.data[segIndex-1].angle, seg.angle)
    }


    // Get settings to rotate vehicle in desired direction as per segment data
    // Rotate slowly
    //var speedDirectionRotate = setSpeedDirection(degDir.turnDirection, rotationSpeed);
    //var hBridgeSettingsRotate = _.extend(defaultHBridgeSettings, speedDirectionRotate)

    console.log('About to rotate vehicle for segment ', segIndex)

    // Rotate the vehicle as per segment data
    rotateVehicle(seg, function(){
        console.log('Rotation completed for segment ', segIndex)

        // Rotation towards the desired angle is completed. Now move forward
        var speedDirection = setSpeedDirection('forward', seg.speed);
        var hBridgeSettings = _.extend(globals.defaultHBridgeSettings, speedDirection);

        // Drive the vehicle for the given segment
        driveVehicle_automatic(hBridgeSettings, seg, function(){
            console.log('Drive completed for segment', segIndex);

            // Run next segment
            segIndex++;
            process.nextTick(runSegment);

        });

    })
}

// Gives the distance between two angles and best direction to turn the vehicle to get to new angle.
function getNewAngleDistanceAndDirection(currentAngle, newAngle){
    //if(currentAngle==0)currentAngle=360;    // to make the logic work
    //if(newAngle==0)newAngle=360;

    currentAngle = parseInt(currentAngle);
    newAngle = parseInt(newAngle);

    var direction;

    // Get the number of degrees the vehicle has to turn to reach the new angle.
    var distanceBetweenTwoAngles = int180 - Math.abs(Math.abs(currentAngle - newAngle) - int180);

    // Get the other side of the current angle to split the circle in half. This helps to determine which direction to travel to reach newAngle.
    var oppositeOfCurrentAngle = (currentAngle <= int180)? (int180 + currentAngle) : (currentAngle - int180);  // Get the opposite degree for currentAngle to split the circle in half
    // Get the direction to turn to reach the new angle.
    if(currentAngle <= int180){
        direction = (newAngle >= currentAngle && newAngle <= oppositeOfCurrentAngle)? 'right':'left';
    }
    else{
        direction = (newAngle <= currentAngle && newAngle >= oppositeOfCurrentAngle)? 'left':'right';
    }

    //console.log('Re-calculating turn direction: Current', currentAngle, 'New', newAngle, 'diff', distanceBetweenTwoAngles, 'opp angle', oppositeOfCurrentAngle, 'direction', direction);
    //console.log('Re-calculating turn direction...')


    return {turnDegrees:distanceBetweenTwoAngles, turnDirection:direction};
}

// Set HBridge values to controls motors direction and speed
function setSpeedDirection(direction, speed1, speed2){
    globals.currentDirection = direction;

//    console.log('Direction:', direction.toUpperCase(), ' Speed1:', speed1);
    
    var hBridgeData ={};
    speed1 = Math.max(0,speed1);  // negative numbers not allowed

    if(speed1 >=0){
        hBridgeData.PWM1Val=speed1;
        hBridgeData.PWM2Val= (speed2 || speed1);
    }

    if(direction=='up' || direction=='forward'){
        hBridgeData.IN1State=1;
        hBridgeData.IN2State=0;
        hBridgeData.IN3State=1;
        hBridgeData.IN4State=0;
    }
    else if(direction=='down' || direction=='backward'){
        hBridgeData.IN1State=0;
        hBridgeData.IN2State=1;
        hBridgeData.IN3State=0;
        hBridgeData.IN4State=1;
    }
    else if(direction=='left'){
        hBridgeData.IN1State=1;
        hBridgeData.IN2State=0;
        hBridgeData.IN3State=0;
        hBridgeData.IN4State=1;
    }
    else if(direction=='right'){
        hBridgeData.IN1State=0;
        hBridgeData.IN2State=1;
        hBridgeData.IN3State=1;
        hBridgeData.IN4State=0;
    }
    else if(direction=='wide_left') {
        hBridgeData.IN1State=1;
        hBridgeData.IN2State=0;
        hBridgeData.IN3State=0;
        hBridgeData.IN4State=0;

        hBridgeData.PWM1Val=0;  // Stop left wheel
        //hBridgeData.PWM2Val=0;

    }
    else if(direction=='wide_right') {
        hBridgeData.IN1State=0;
        hBridgeData.IN2State=0;
        hBridgeData.IN3State=1;
        hBridgeData.IN4State=0;

        hBridgeData.PWM2Val=0;  // Stop right wheel

    }
    else if(direction=='stop'){
        hBridgeData.IN1State=0;
        hBridgeData.IN2State=0;
        hBridgeData.IN3State=0;
        hBridgeData.IN4State=0;

        hBridgeData.PWM1Val=0;
        hBridgeData.PWM2Val=0;
    }

    return hBridgeData;
}

function rotateVehicle(seg, cb){

    // Publish status to client
//    var updateInterval = setInterval(function(){
//        // Update client
//        var seg2 = JSON.parse( JSON.stringify(seg) );
//
////        HBridge.publishUpdate(hBridgeID, {tripSegment: _.extend(seg2,{index:segIndex, direction:'rotating', speed:rotationSpeed, currentDist: 0, currentDeg: globals.compassAngle})});
//    },500);
    
    // Rotate vehicle until desired angle is reached
    var setRotation=true;
    var currentDiff=0;
    var prevDiff=361;   // Keep the initial value more than 360 to make the logic work.
    var degDir;
    var angleMarginOfError = 5;     // When the vehicle rotates to reach desired new angle, the rotation stops when the difference between current angle and desired angle is +/- angleMarginOfError.  This helps the vehicle to complete the rotation and start moving.  The difference of angle will be corrected automatically (in driveVehicle_automatic()) as vehicle moves.
    newAngleMissCount = 0;

    var segInterval = setInterval(function(){

        // Set the vehicle in rotation
        if(setRotation){
            // Find out which direction to rotate to reach the new angle quickly
            degDir = getNewAngleDistanceAndDirection(globals.compassAngle, seg.angle)

            console.log('degDir', degDir)
            
            // Get settings to set hBridge to drive vehicle as desired
            var speedDirectionRotate = setSpeedDirection(degDir.turnDirection, rotationSpeed);
            globals.currentSpeedDirection = _.extend(globals.defaultHBridgeSettings, speedDirectionRotate)


            // Update hbridge settings
            driveVehicle_manual(globals.currentSpeedDirection);
            
            setRotation=false;
        }

        // If the vehicles misses the new desired angle multiple times as it rotates, reduce the rotation speed
        // auto deceleration if vehicle is rotating too fast
        if(newAngleMissCount > 2){
            console.log('Vehicle rotating too fast. Reducing the speed.');
            rotationSpeed = rotationSpeed - 5;
            newAngleMissCount = 0;
            setRotation = true;             // This makes the new speed applied to hbridge during next setInterval iteration
        }

        // If vehicle is not moving, increase the speed
        // auto acceleration if vehicle is struggling to rotate as speed is not enough.
        if( !globals.isVehicleMoving() ) {
            console.log('Vehicle is not rotating. Increasing the speed.');
            rotationSpeed = rotationSpeed + 5;
            setRotation = true;
        }


        // console.log('Compass angle:', globals.compassAngle, 'Desired angle:', seg.angle)

        // Check whether the rotation passed the desired angle, if so, rotate opposite direction until desired angle is reached
        // Get the difference between compass angle and desired new angle (seg.angle).  It should be receding gradually as the vehicle reaches the desired angle.
        currentDiff = int180 - Math.abs(Math.abs(globals.compassAngle - seg.angle) - int180);

        // If the vehicle is turning in desired direction to reach the desired angle, currentDiff is always less than prevDiff
        // When the turn reaches the desired angle, the currentDiff should be 0.
        // If the vehicle misses the desired angle and keep turning, then currentDiff will become grater than prevDiff
        if(currentDiff <= prevDiff){
//            HBridge.message(hBridgeID, {comments: 'Turning ' + currentDiff + ' degrees ' + degDir.turnDirection + ' to reach ' + seg.angle + ' degrees'});
            globals.comments = 'Turning ' + currentDiff + ' degrees ' + degDir.turnDirection.toUpperCase() + ' to reach ' + seg.angle + ' degrees';
//            console.log(globals.comments);
            prevDiff=currentDiff;
        }
        else{   // This becomes true when the vehicle missed the desired angle.  Re-evaluate the turn direction again to reach the desired angle
            //console.log('Rotation missed the ' + seg.angle + ' angle by', currentDiff, 'degrees')
            prevDiff=361;
            newAngleMissCount++;
            setRotation=true;   // Alter vehicle rotation to reach the desired angle ( in this case it should turn opposite direction as it passed the desired angle)
        }

//        if(globals.compassAngle == seg.angle){
        // As the rotation come close to the new desired angle, consider it as rotation completion and let the vehicle move forward in that new direction.
        // Even if the vehicle is not reached desired angle during the rotation, there is logic in place to correct minor orientation issues as the vehicle travels.
        if( Math.abs(globals.compassAngle - seg.angle) <= angleMarginOfError ){

            console.log('Reached the desired angle', seg.angle);
            clearInterval(segInterval);
//            clearInterval(updateInterval);
            cb();
        }

    },100);


};

// Writes new settings to HBridge to change vehicle movement.
function driveVehicle_manual(currentSpeedDirection){
    var hBridgeStng = Object.assign(globals.defaultHBridgeSettings, currentSpeedDirection); // Replace hbridge defaults with new ones

    // Set motor direction
    wpi.digitalWrite(hBridgeStng.IN1Pin, (hBridgeStng.IN1State == 1) ? wpi.HIGH : wpi.LOW);		// Specify GPIO pin number (not the physical pin number);
    wpi.digitalWrite(hBridgeStng.IN2Pin, (hBridgeStng.IN2State == 1) ? wpi.HIGH : wpi.LOW);		// Specify GPIO pin number (not the physical pin number);
    wpi.digitalWrite(hBridgeStng.IN3Pin, (hBridgeStng.IN3State == 1) ? wpi.HIGH : wpi.LOW);		// Specify GPIO pin number (not the physical pin number);
    wpi.digitalWrite(hBridgeStng.IN4Pin, (hBridgeStng.IN4State == 1) ? wpi.HIGH : wpi.LOW);		// Specify GPIO pin number (not the physical pin number);

    // Set motor speed
//    piblaster.setPwm(hBridgeStng.PWM1Pin, hBridgeStng.PWM1Val);									// Specify GPIO pin number (not the physical pin number);
//    piblaster.setPwm(hBridgeStng.PWM2Pin, hBridgeStng.PWM2Val);									// Specify GPIO pin number (not the physical pin number);

//    wpi.pinMode(hBridgeStng.PWM1Pin, wpi.PWM_OUTPUT);       // Pin Mode must be PWM_OUTPUT
//    wpi.pinMode(hBridgeStng.PWM2Pin, wpi.PWM_OUTPUT);       // Pin Mode must be PWM_OUTPUT

//    console.log('globals.leftPWM', globals.leftPWM, hBridgeStng.PWM1Val)

    // pwmWrite values go from 0 to 255, so scale roughly
    hBridgeStng.PWM1Val = Math.min(hBridgeStng.PWM1Val,PWMMax);
    hBridgeStng.PWM2Val = Math.min(hBridgeStng.PWM2Val,PWMMax);

    globals.leftPWM.pwmWrite(hBridgeStng.PWM1Val);    // Set the default, otherwise it is giving error when trying to execute globals.leftPWM.getPwmDutyCycle()
    globals.rightPWM.pwmWrite(hBridgeStng.PWM2Val);

//    console.log('hBridge', hBridgeStng)

    return;
}

function driveVehicle_automatic(hBridge, seg, cb){
    var segCurrentDist=0;
    // Some times a wheel's speed could be 0 but wheel one and two's speed would never be 0
    //currentSpeed = Math.max( parseInt( parseFloat(hBridge.PWM1Val)*100, 10), parseInt( parseFloat(hBridge.PWM2Val)*100, 10));

    driveVehicle_manual(hBridge);
    
    // Monitor distance travelled.
    console.log('Resetting distance travelled for segment', segIndex);
    globals.segmentDistance=0;  // Reset travelled distance for the current segment
    var reducedSpeed=0;

//    var updateInterval = setInterval(function(){
//        // Update client
//        HBridge.publishUpdate(hBridgeID, {tripSegment: _.extend(seg,{index:segIndex, currentDist: segCurrentDist, currentDeg: sails.config.globals.navigation.compassAngle})});
//        HBridge.message(hBridgeID, {comments: 'Travelled ' + segCurrentDist + ' / ' + seg.distance + ' inches on segment ' + (segIndex + 1) });
//    },1000);

    var segInterval = setInterval(function(){
        // Get the distance travelled
        //segCurrentDist = (sails.config.globals.encoder.inchesPerIteration * sails.config.globals.encoder.iterationCount);
//        segCurrentDist = globals.inchCount;

        // Keep an eye on the angle that the vehicle is supposed to travel.
        // There is a chance that the vehicle could go off for many reasons.
        // If the vehicle angle is off, reduce the speed of respective motor to gracefully bring the vehicle back to its desired angle

        // Find out whether the vehicle is off of its designated angle
        degDir = getNewAngleDistanceAndDirection(globals.compassAngle, seg.angle)
        if(degDir.turnDegrees != 0){    // If the vehicle is off

            if(degDir.turnDirection == 'left'){
                // Reduce left motor speed by  motorSpeedReduction percent to bring the vehicle back to right angle.
                reducedSpeed = hBridge.PWM2Val - ( hBridge.PWM2Val * motorSpeedReduction)/100
//                piblaster.setPwm(hBridge.PWM2Pin, reducedSpeed);									// Specify GPIO pin number (not the physical pin number);
                driveVehicle_manual(hBridge);

                console.log('Reduced LEFT motor speed by ', motorSpeedReduction, '% to turn ', degDir.turnDegrees, ' degrees');
            }
            else{
                // Reduce right motor speed by motorSpeedReduction percent to bring the vehicle back to right angle.
                reducedSpeed = hBridge.PWM1Val - ( hBridge.PWM1Val * motorSpeedReduction)/100
//                piblaster.setPwm(hBridge.PWM1Pin, reducedSpeed);									// Specify GPIO pin number (not the physical pin number);
                driveVehicle_manual(hBridge);

                console.log('Reduced RIGHT motor speed by ', motorSpeedReduction, '% to turn ', degDir.turnDegrees, ' degrees');
            }
        }
        else{
            // Reset to equal speed for all motors when the vehicle is going in right angle.
//            piblaster.setPwm(hBridge.PWM1Pin, hBridge.PWM1Val);									// Specify GPIO pin number (not the physical pin number);
//            piblaster.setPwm(hBridge.PWM2Pin, hBridge.PWM2Val);									// Specify GPIO pin number (not the physical pin number);

            hBridge.PWM1Val = seg.speed;
            hBridge.PWM2Val = seg.speed;
        }

        console.log(globals.segmentDistance.toFixed(2), 'of', seg.distance, ' inches travelled on this Segment')

        //if(segCurrentDist >= parseFloat(seg.distance)){
        if(globals.segmentDistance >= parseFloat(seg.distance)){
            globals.comments = 'Travelled ' + globals.segmentDistance + ' / ' + seg.distance + ' inches on segment ' + (segIndex + 1);
            console.log(globals.comments)
            clearInterval(segInterval);
//            clearInterval(updateInterval);
            cb();
        }

    },100);

}

function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}



console.log('Navigation module is loaded.');