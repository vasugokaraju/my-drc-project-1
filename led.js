//var wpi= require('wiring-pi');

var pigpio=require('pigpio');
var pG= pigpio.Gpio;


//wpi.setup('gpio');

var led1 = new pG(23, {mode:pG.OUTPUT});

//led1.pwmWrite(90);

var brightness = 0;
var maxBrightness = 255;

var intervalHandler;


intervalHandler = setInterval( blinkLED,50);

// Blink LED
var litStatus = false;
function blinkLED(){

    if(litStatus == true){
        litStatus = false;
        led1.pwmWrite(10);
    }
    else{
        litStatus = true;
        led1.pwmWrite(250);
    }

    if(brightness >= maxBrightness){
        console.log('Reached max brightness');
        clearInterval(intervalHandler);
    }
}


// Gradually increases brightness of LED.
function gradualBrightness(){

    brightness =+ 20;
    led1.pwmWrite(brightness);

    console.log('Brightness:', brightness);

    if(brightness >= maxBrightness){
        console.log('Reached max brightness');
        clearInterval(intervalHandler);
    }
}
