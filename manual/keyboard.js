/**
 * Created by vasu on 10/26/17.
 */

var keypress = require('keypress');
var nav = require('../common/navigation');
var globals = require('../common/globals');
var google = require('../automatic/google');


// make `process.stdin` begin emitting "keypress" events
keypress(process.stdin);

// listen for the "keypress" event
process.stdin.on('keypress', function (ch, key) {
    if(!key) return;
    console.log('got keypress', key);
//    console.log('You pressed the key "', key.sequence, '"')

    if(key.shift){
        key.name = key.sequence;    // sequence gives case sensitive letter
    }

    switch(key.name){
        case 'left':
        case 'right':
        case 'up':
        case 'down':
        case 'escape':  // Stop
        case 'j':   // Left (Left wheels rotate backward, Right wheels rotate forward)
        case 'l':   // Right
        case 'i':   // Up
        case 'k':   // Down
        case 'q':   // Stop
        case 'J':   // Wide Left (Left wheels are locked)
        case 'L':   // Wide Right (Right wheels are locked)
            nav.drive(key.name)
            break;
        case 'a':
            nav.speed(5);
            break;
        case 'd':
            nav.speed(-5);
            break;
    }

    // Exit when Ctrl+C is pressed
    if (key && key.ctrl && key.name == 'c') {
//        process.stdin.pause();
        nav.drive('q'); // stop if running
        globals.clearConsole();
        console.log('\n\nRobot is stopped. Program is terminated.\n\n')
        google.updateJobStatus('Stop')
        process.exit(0);
    }
});

process.stdin.setRawMode(true);
process.stdin.resume();


console.log('Keyboard module is loaded.\n')

console.log('Keyboard Help:')
console.log('Left Arrow/J to turn Left.');
console.log('Right Arrow/L to turn Right.');
console.log('Up Arrow/I to go Forward.');
console.log('Down Arrow/K to go Backward.');
console.log('Esc/Q to Stop.');
console.log('Ctrl + C to exit robot operating system.');
